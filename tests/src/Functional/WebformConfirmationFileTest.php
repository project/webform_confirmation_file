<?php

namespace Drupal\Tests\webform_confirmation_file\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Webform confiramtion file browser test.
 *
 * @group webform_browser
 */
class WebformConfirmationFileTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['webform', 'webform_confirmation_file', 'webform_confirmation_file_test'];

  /**
   * Set default theme to stable.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Test confirmation file handler.
   */
  public function testConfirmationFileHandler() {
    $this->drupalGet('/webform/test_confirmation_file_content');
    // Check content source.
    $this->submitForm([], t('Submit'));
    $this->assertSession()->responseContains('<?xml version="1.0"?>');
    $this->drupalGet('/webform/test_confirmation_file_url');

    // Check URI source.
    $this->submitForm([], t('Submit'));
    $this->assertSession()->responseContains('<?xml version="1.0"?>');

  }

}
