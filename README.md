Webform Confirmation File 8.x-1.x
---------------------------------

### About this Module

The Webform Confirmation file module provides a webform handler that streams 
the contents of a file to a user after completing a webform.
