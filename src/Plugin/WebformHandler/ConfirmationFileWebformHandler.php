<?php

namespace Drupal\webform_confirmation_file\Plugin\WebformHandler;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Streams the contents of a file to a user after completing a webform.
 *
 * @WebformHandler(
 *   id = "confirmation_file",
 *   label = @Translation("Confirmation file"),
 *   category = @Translation("Confirmation"),
 *   description = @Translation("Streams the contents of a file after submitting a webform."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED
 * )
 */
class ConfirmationFileWebformHandler extends WebformHandlerBase {

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypesInterface
   */
  protected $mimeTypeGuesser;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->mimeTypeGuesser = $container->get('file.mime_type.guesser.extension');
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'name' => '',
      'source' => 'content',
      'url' => '',
      'content' => '',
      'download' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    $summary['#settings']['source'] = ($summary['#settings']['source'] === 'url')
      ? t('URL/path')
      : t('Content/text');
    $summary['#settings']['mimetype'] = $this->getMimeType();
    $summary['#settings']['filesize'] = format_size($this->getFileSize());
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('File settings'),
    ];
    $form['file']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File name with extension'),
      '#default_value' => $this->configuration['name'],
      '#required' => TRUE,
    ];
    $form['file']['source'] = [
      '#type' => 'radios',
      '#title' => $this->t('File source'),
      '#options' => [
        'url' => $this->t('URL/path'),
        'content' => $this->t('Content/text'),
      ],
      '#options_description_display' => 'help',
      '#default_value' => $this->configuration['source'],
      '#required' => TRUE,
    ];
    $form['file']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File URL/path'),
      '#description' => $this->t("Make sure the attachment URL/Path is publicly accessible. The attachment's URL/path will never be displayed to end users."),
      '#required' => TRUE,
      '#default_value' => $this->configuration['url'],
      '#element_validate' => [[get_class($this), 'validateFileUrl']],
      '#states' => [
        'visible' => [':input[name="settings[source]"]' => ['value' => 'url']],
        'required' => [':input[name="settings[source]"]' => ['value' => 'url']],
      ],
    ];
    if (function_exists('imce_process_url_element')) {
      $url_element = &$form['file']['url'];
      imce_process_url_element($url_element, 'link');
      $form['#attached']['library'][] = 'webform/imce.input';
    }
    $form['file']['content'] = [
      '#type' => 'webform_codemirror',
      '#title' => $this->t('File content/text'),
      '#default_value' => $this->configuration['content'],
      '#states' => [
        'visible' => [':input[name="settings[source]"]' => ['value' => 'content']],
        'required' => [':input[name="settings[source]"]' => ['value' => 'content']],
      ],
    ];
    $form['file']['download'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force the user to download the file'),
      '#default_value' => $this->configuration['download'],
      '#return_value' => TRUE,
    ];

    $this->setSettingsParentsRecursively($form);
    $this->tokenManager->elementValidate($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['name'] = $form_state->getValue('name');
    $this->configuration['source'] = $form_state->getValue('source');
    $this->configuration['url'] = $form_state->getValue('url');
    $this->configuration['content'] = $form_state->getValue('content');
    $this->configuration['download'] = $form_state->getValue('download');
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Replace all tokens in the configuration settings.
    $this->configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    $filename = $this->configuration['name'];
    $attachment = ($this->configuration['download'] ? 'attachment;' : '');

    $headers = [
      'Content-Length' => $this->getFileSize(),
      'Content-Type' => $this->getMimeType(),
      'Content-Disposition' => $attachment . 'filename="' . $filename . '"',
    ];

    $response = new Response($this->getFileContent(), 200, $headers);
    $form_state->setResponse($response);
  }

  /**
   * Get the mime type from the file name.
   *
   * @return string
   *   The mime type.
   */
  protected function getMimeType() {
    return $this->mimeTypeGuesser->guess($this->configuration['name']);
  }

  /**
   * Get the file size from the file content.
   *
   * @return int
   *   The file size.
   */
  protected function getFileSize() {
    return mb_strlen($this->getFileContent(), '8bit');
  }

  /**
   * Get the file size from the file content.
   *
   * @return int
   *   The file size.
   */
  protected function getFileContent() {
    if ($this->configuration['source'] === 'content') {
      return $this->configuration['content'];
    }
    else {
      try {
        $url = $this->configuration['url'];
        $url = \Drupal::service('file_url_generator')->generateAbsoluteString($url) ?: $url;
        if (strpos($url, '/') === 0) {
          $url = \Drupal::request()->getSchemeAndHttpHost() . $url;
        }
        return (string) \Drupal::httpClient()->get($url)->getBody();
      }
      catch (RequestException $exception) {
        return '';
      }
    }
  }

  /**
   * Form API callback. Validate url/path.
   *
   * @see \Drupal\Core\Render\Element\Url::validateUrl
   */
  public static function validateFileUrl(&$element, FormStateInterface &$form_state) {
    $value = trim($element['#value']);
    $form_state->setValueForElement($element, $value);

    // Prepend scheme and host to root relative path.
    if (strpos($value, '/') === 0) {
      $value = \Drupal::request()->getSchemeAndHttpHost() . $value;
    }

    // Skip validating [webform_submission] tokens which can't be replaced.
    if (strpos($value, '[webform_submission:') !== FALSE) {
      return;
    }

    // Validate URL formatting.
    if ($value !== '' && !UrlHelper::isValid($value, TRUE)) {
      $form_state->setError($element, t('The URL %url is not valid.', ['%url' => $value]));
    }

    // Validate URL access.
    try {
      \Drupal::httpClient()->head($value);
    }
    catch (ClientException $e) {
      $form_state->setError($element, t('The URL <a href=":url">@url</a> is not available.', [':url' => $value, '@url' => $value]));
    }
  }

}
